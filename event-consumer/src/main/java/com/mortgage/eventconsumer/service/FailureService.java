package com.mortgage.eventconsumer.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class FailureService {
    public void saveFailedRecord(ConsumerRecord<Integer, String> record, Exception exception, String retry) {
        //Saves Record to db
    }
}
