package com.mortgage.eventconsumer.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mortgage.eventconsumer.model.MortgageEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EventsConsumerService {

    @Autowired
    ObjectMapper objectMapper;

    public void processMortgageEvent(ConsumerRecord<Integer, String> consumerRecord) throws JsonProcessingException {

        MortgageEvent mortgageEvent = objectMapper.readValue(consumerRecord.value(), MortgageEvent.class);

        log.info("Mortgage Event received : {} ", mortgageEvent);
        //Perform application logic

    }
}
