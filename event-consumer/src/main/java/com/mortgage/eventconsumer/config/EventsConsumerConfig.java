package com.mortgage.eventconsumer.config;

import com.mortgage.eventconsumer.service.FailureService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.kafka.ConcurrentKafkaListenerContainerFactoryConfigurer;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.*;
import org.springframework.kafka.support.ExponentialBackOffWithMaxRetries;
import org.springframework.util.backoff.FixedBackOff;

import javax.naming.ServiceUnavailableException;

@Configuration
@EnableKafka
@Slf4j
public class EventsConsumerConfig {

    public static final String RETRY = "RETRY";
    public static final String DEAD = "DEAD";

    @Autowired
    KafkaProperties kafkaProperties;

    @Autowired
    KafkaTemplate kafkaTemplate;

    @Value("${topics.retry}")
    private String retryTopic;

    @Value("${topics.dlt}")
    private String deadLetterTopic;

    @Autowired
    FailureService failureService;


    public DeadLetterPublishingRecoverer publishingRecoverer() {

        DeadLetterPublishingRecoverer recoverer = new DeadLetterPublishingRecoverer(kafkaTemplate
                , (r, e) -> {
            log.error("Exception encountered during processing message : {} ", e.getMessage(), e);
            if (e.getCause() instanceof ServiceUnavailableException) {
                return new TopicPartition(retryTopic, r.partition());
            } else {
                return new TopicPartition(deadLetterTopic, r.partition());
            }
        }
        );

        return recoverer;
    }

    public ConsumerRecordRecoverer consumerRecordRecoverer() {
        ConsumerRecordRecoverer consumerRecordRecoverer = (record, exception) -> {
            log.error("Exception is : {} Failed Record : {} ", exception, record);
            if (exception.getCause() instanceof ServiceUnavailableException) {
                log.info("Inside the recoverable error record {}", record);
                failureService.saveFailedRecord((ConsumerRecord<Integer, String>) record, exception, RETRY);
            } else {
                log.info("Inside the non recoverable error record {}", record);
                failureService.saveFailedRecord((ConsumerRecord<Integer, String>) record, exception, DEAD);
            }
        };
        return consumerRecordRecoverer;
    }

    public DefaultErrorHandler errorHandlerWithExpBackOffRetry() {

        ExponentialBackOffWithMaxRetries exponentialBackOff = new ExponentialBackOffWithMaxRetries(2);
        exponentialBackOff.setInitialInterval(1000);
        exponentialBackOff.setMultiplier(2.0);
        exponentialBackOff.setMaxInterval(3000);

        DefaultErrorHandler defaultErrorHandler = new DefaultErrorHandler(publishingRecoverer(), exponentialBackOff);

        defaultErrorHandler.addRetryableExceptions(ServiceUnavailableException.class);

        defaultErrorHandler.setRetryListeners(
                (record, ex, deliveryAttempt) ->
                        log.info("Failed Record in Retry Listener  exception : {} , deliveryAttempt : {} ", ex.getMessage(), deliveryAttempt)
        );

        return defaultErrorHandler;
    }

    public DefaultErrorHandler errorHandlerWithFixedBackOffRetry() {

        FixedBackOff fixedBackOff = new FixedBackOff(1000L, 2L);
        DefaultErrorHandler defaultErrorHandler = new DefaultErrorHandler(fixedBackOff);

        defaultErrorHandler.addRetryableExceptions(ServiceUnavailableException.class);

        defaultErrorHandler.setRetryListeners(
                (record, ex, deliveryAttempt) ->
                        log.info("Failed Record in Retry Listener  exception : {} , deliveryAttempt : {} ", ex.getMessage(), deliveryAttempt)
        );

        return defaultErrorHandler;
    }

    @Bean
    @ConditionalOnMissingBean(name = "kafkaListenerContainerFactory")
    ConcurrentKafkaListenerContainerFactory<?, ?> kafkaListenerContainerFactory(
            ConcurrentKafkaListenerContainerFactoryConfigurer configurer,
            ObjectProvider<ConsumerFactory<Object, Object>> kafkaConsumerFactory) {
        ConcurrentKafkaListenerContainerFactory<Object, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
        configurer.configure(factory, kafkaConsumerFactory
                .getIfAvailable(() -> new DefaultKafkaConsumerFactory<>(this.kafkaProperties.buildConsumerProperties())));
        factory.setCommonErrorHandler(errorHandlerWithExpBackOffRetry());
        return factory;
    }
}