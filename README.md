## Overview

Apache `Kafka` is an open-source distributed streaming system used for stream processing, real-time data pipelines and data integration at scale. It offers high availability, fault tolerance, reliable work flow distribution and scalability.

### Important Kafka terminologies explained


#### Important Kafka terminologies explained

##### Important Kafka terminologies explained


Kafka Cluster: Group of multiple Kafka brokers.

Kafka Broker: A server that stores and manages messages.

Topics: They are categories used to organise messages. Each topic has a name that is unique across the entire Kafka cluster. Messages are sent to and read from specific topics

Partitions: Topics are divided into several partitions. While the topic is a logical concept in Kafka, a partition is the smallest storage unit that holds a subset of records owned by a topic.

Offsets: The records in the partitions are each assigned a sequential identifier called the offset which is unique for each record within the partition.

Producer: Client applications that write messages to Kafka topics.

Consumer: Client applications that subscribe to one or multiple topics to read messages from topics.

Consumer group: A group of consumers that share the same group id. When a topic is consumed by consumers in the same group, every record will be delivered to only one consumer.

Replication/Replication factor: Number of copies of partition across the Kafka cluster.

 

This site(https://softwaremill.com/kafka-visualisation/ ) gives a nice visualisation of how all the above components working together, you can play around with the configuration on the left pane and see how the changes impact the behaviour of Kafka.

Kafka Producer

Kafka Producer component uses the ProducerAPI to send events to Kafka Broker. Below are some important points that should be considered while designing a Kafka Producer

Message Availability and Durability

When sending events to Kafka, producers can choose whether they wait for the message to be acknowledged or not. This is controlled by producer configuration acks  

acks = 0  producer api returns success after message send is completed. It doesn't wait for the acknowledgement from broker on message writes to replica’s. Message loss might occur if this option is set.

acks = 1 acknowledgement received when message is persisted to leader replica.

acks = -1 or all acknowledgement received when message is persisted to leader replica and all follower replica’s.

It is advisable to have acks set to all(Default setting in Kafka >= v3.0). 

Replication factor(In Sync Replicas)

Number of copies of message that gets stored across the broker, its advisable to set this value to > 1 when creating the topic. 

min.insync.replicas specifies the minimum number of replicas that must acknowledge a write for the message send to be considered successful. Set this value to > 1 to make sure that messages are always written at least to 1 follower replica.

A recommended setting would be to create a topic with a replication factor of 3, set min.insync.replicas to 2, and produce with acks of "all". This will ensure that the producer raises an exception if a majority of replicas do not receive a write. Here min.insync.replicas and acks allow you to enforce greater durability guarantees. 

Idempotency

When a producer send a event to kafka the broker should acknowledge(when acks set to ==1 or all) that the message has been received in Kafka. There can be a scenario where the message was successfully written and replicated but the acknowledgement never reached producer due to a intermittent Network Issue. In this instance producer retry logic will be invoke which will result in message duplication. This can be avoided and message idempotency can be achieved by  setting enable.idempotence = true

Retry

Message send from a producer to the Kafka broker can fail as a result of following reasons

Kafka cluster is down and unavailable.

If Kafka producer configuration “acks” is configured to “all” and some brokers are unavailable.

If Kafka producer configuration “min.insyn.replicas” is specified as 2 and only one broker is available. 

When a producer message send is failed the producer api will retry the message based on producer configuration retries, this value is set to Integer.MAX_VALUE by default and it can be left as-is.

Retries will stop when delivery.timeout.ms is reached and by default it is set to 120000ms and it can be left as-is.

We can control the intervals between every retry using retry.backoff.ms configuration, this is defaulted to 100ms. 

Recovery and replay

After all the message retry’s are exhausted following option’s can be used to implement the replay of failed messages.



Option 1:



Failed messages are sent to a Retry topic, a retry consumer component polls this topic and will try to resend the message through the Producer component. Refer to : 


 
![alt text](file/producer_replay_option1.png "Producer replay Option 1")



Option 2:


![alt text](file/producer_replay_option2.png "Producer replay Option 2")

Failed messages are persisted to a Retry table in database, a Spring scheduler component polls this topic periodically and will try to resend the message through the Producer component. Refer to : 




Option 2 to persist the message to database is the recommended option as the replay of message is controlled by the scheduler which gives sometime for underlying issue to resolve.

Note: If the requirement is to send message with a key to make them write to the same partition for maintaining order of insertion replay might affect the message order.

Kafka Consumer

Retry

Message received from Kafka topic can fail as a result of various reasons like down stream service unavailability, data validation issue, parser error etc.,

Refer to : 
We can use retry or not retry a specific exception type by creating a custom CommonErrorHandler and setting the same in ConcurrentKafkaListenerContainerFactory instance

Below code from EventConsumerConfig retries ServiceUnavailableException 3 times using a fixed back off with 1000 ms interval. You can also update defaultErrorHandler.addRetryableException with exceptions that should be skipped from retry logic.

   



Retry logic with a exponential back off below

  Refer to:  

Note: defaultErrorHandler.setRetryListeners is used to intercept the failure in each retry, this can be used to log additional information on every retry failure.

Recovery and replay

After all the message retry’s are exhausted following option’s can be used to implement the replay of failed messages.

Option 1: Using DeadLetterPublishingRecoverer see: https://docs.spring.io/spring-kafka/reference/kafka/annotation-error-handling.html#:~:text=about%20consuming%20batches.-,Publishing%20Dead%2Dletter%20Records,failed%20message%20to%20another%20topic. 


![alt text](file/consumer_replay_option1.png "Consumer replay Option 1")

Failed messages are sent to a Retry topic (only if they are recoverable), a retry consumer component polls this topic and will try to re-process the message through the Service component. All the messages that failed due to non-recoverable exceptions are redirected to a DeadLetter topic from where they can be analysed. Below is a code snipper of Event ConsumerConfig class




Option 2: Using ConsumerRecordRecoverer

![alt text](file/consumer_replay_option2.png "Consumer replay Option 2")


Failed messages are sent to a Retry db table (only if they are recoverable), a spring scheduler component polls this topic periodically and will try to re-process the message through the Service component. All the messages that failed due to non-recoverable exceptions are redirected to a DeadLetter topic from where they can be analysed. Below is a code snipper of Event ConsumerConfig class




Note: Its very important to push only the recoverable failed messages to the Retry topic/db table. Else we might introduce a infinite loop condition where the message will always fail.

Kafka Streams

Work-in-Progress

Reactive Kafka Producer/Consumer with SpringBoot

Work-in-Progress




